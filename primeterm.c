#include <vte/vte.h>

#define CLR_R(x)   (((x) & 0xff0000) >> 16)
#define CLR_G(x)   (((x) & 0x00ff00) >>  8)
#define CLR_B(x)   (((x) & 0x0000ff) >>  0)
#define CLR_16(x)  ((double)(x) / 0xff)
#define CLR_GDK(x) (const GdkRGBA){ .red = CLR_16(CLR_R(x)), \
                                    .green = CLR_16(CLR_G(x)), \
                                    .blue = CLR_16(CLR_B(x)), \
                                    .alpha = 0 }

int main(int argc, char *argv[]) {
	GtkWidget *window, *terminal;

	/* Initialise GTK, the window, and the terminal */
	gtk_init(&argc, &argv);
	terminal = vte_terminal_new();
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "primeterm");

	vte_terminal_set_colors(VTE_TERMINAL(terminal),
    		&CLR_GDK(0xffffff),
    		&(GdkRGBA){ .alpha = 0.85 },
    		(const GdkRGBA[]){
        		CLR_GDK(0x111111),
        		CLR_GDK(0xff2200),
        		CLR_GDK(0x95ff00),
        		CLR_GDK(0xffd800),
        		CLR_GDK(0x008dff),
        		CLR_GDK(0x743c96),
        		CLR_GDK(0x5e7175),
        		CLR_GDK(0xbebebe),
        		CLR_GDK(0x666666),
        		CLR_GDK(0xff4300),
        		CLR_GDK(0x87ba09),
        		CLR_GDK(0xffec00),
        		CLR_GDK(0x74b8ef),
        		CLR_GDK(0x8c5eb8),
        		CLR_GDK(0xa3babf),
        		CLR_GDK(0xffffff)
		}, 16);

	/* Start a new shell */
	gchar **envp = g_get_environ();
	gchar **command = (gchar *[]){g_strdup(g_environ_getenv(envp, "SHELL")), NULL};
	g_strfreev(envp);
	vte_terminal_spawn_sync(VTE_TERMINAL(terminal),
			VTE_PTY_DEFAULT,
			NULL,        // Working directory
			command,     // Command
			NULL,        // Environment
			0,           // Spawn flags
			NULL, NULL,  // Child setup
			NULL,        // Child PID
			NULL, NULL);

	/* Connect some signals */
	g_signal_connect(window, "delete-event", gtk_main_quit, NULL);
	g_signal_connect(terminal, "child-exited", gtk_main_quit, NULL);

	/* Put widgets together and run the main loop */
	gtk_container_add(GTK_CONTAINER(window), terminal);
	gtk_widget_show_all(window);
	gtk_main();

	/* Specific settings */
	vte_terminal_set_scrollback_lines(VTE_TERMINAL(terminal), 0);        // Disable scrollback buffer
	//vte_terminal_set_scroll_on_output(VTE_TERMINAL(terminal), FALSE);    // Disable scrolling-to-bottom on new output
	vte_terminal_set_scroll_on_keystroke(VTE_TERMINAL(terminal), TRUE);  // Scroll to bottom on keystroke
	vte_terminal_set_rewrap_on_resize(VTE_TERMINAL(terminal), TRUE);     // Rewrap content on terminal size changes
	//vte_terminal_set_mouse_autohide(VTE_TERMINAL(terminal), TRUE);       // Hide mouse cursor when typing
}
